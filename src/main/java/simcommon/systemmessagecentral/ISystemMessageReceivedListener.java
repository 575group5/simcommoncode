package simcommon.systemmessagecentral;

import simcommon.messages.systemMessages.SystemMessage;

public interface ISystemMessageReceivedListener {
	void onMessageReceived(SystemMessage message);
}
