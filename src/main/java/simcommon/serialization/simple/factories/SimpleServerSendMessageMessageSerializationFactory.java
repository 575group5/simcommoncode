package simcommon.serialization.simple.factories;

import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.ISpecificMessageSerializerDeserializerFactory;
import simcommon.serialization.simple.deserializers.SimpleServerSendMessageMessageDeserializer;
import simcommon.serialization.simple.serializers.SimpleServerSendMessageMessageSerializer;

public class SimpleServerSendMessageMessageSerializationFactory implements ISpecificMessageSerializerDeserializerFactory {

	@Override
	public IIndividualMessageSerializer getSpecificSerializer() {
		return new SimpleServerSendMessageMessageSerializer();
	}

	@Override
	public IIndividualMessageDeserializer getSpecificDeserializer() {
		return new SimpleServerSendMessageMessageDeserializer();
	}

}
