package simcommon.serialization.simple.factories;

import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.ISpecificMessageSerializerDeserializerFactory;
import simcommon.serialization.simple.deserializers.SimpleServerSuccessResponseMessageDeserializer;
import simcommon.serialization.simple.serializers.SimpleServerSuccessResponseMessageSerializer;

public class SimpleServerSuccessMessageSerializationFactory implements ISpecificMessageSerializerDeserializerFactory {

	@Override
	public IIndividualMessageSerializer getSpecificSerializer() {
		return new SimpleServerSuccessResponseMessageSerializer();
	}

	@Override
	public IIndividualMessageDeserializer getSpecificDeserializer() {
		return new SimpleServerSuccessResponseMessageDeserializer();
	}

}
