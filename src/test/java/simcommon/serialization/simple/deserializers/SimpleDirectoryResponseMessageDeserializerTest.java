package simcommon.serialization.simple.deserializers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.DeserializationException;

public class SimpleDirectoryResponseMessageDeserializerTest {
	SimpleDirectoryResponseMessageDeserializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleDirectoryResponseMessageDeserializer();
	}
	
	@Test
	public void testReadFromInput() throws DeserializationException{
		SystemMessage inputMessage = testObj.readFromInput("{DirectoryRespose-Simple:test,conversationsList:[]}");
		assertTrue(inputMessage instanceof DirectoryResponseSystemMessage);
	}
	
	@Test
	public void testCanHandle(){
		assertFalse(testObj.canHandle("{DirectoryRespose-Simple:test}"));
	}

}
