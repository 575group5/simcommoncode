package simcommon.serialization;

public interface IIndividualMessageDeserializer extends IMessageDeserializer{

	boolean canHandle(String input);
}
