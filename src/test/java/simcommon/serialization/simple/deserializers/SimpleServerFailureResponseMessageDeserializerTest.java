package simcommon.serialization.simple.deserializers;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;

public class SimpleServerFailureResponseMessageDeserializerTest {
	SimpleServerFailureResponseMessageDeserializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleServerFailureResponseMessageDeserializer();
	}
	
	@Test
	public void testReadFromInput(){
		SystemMessage inputMessage = testObj.readFromInput("USER_NOT_AUTHENTICATED");
		assertTrue(inputMessage instanceof GeneralServerFailureResponseSystemMessage);
	}
	
	@Test
	public void testCanHandle(){
		assertTrue(testObj.canHandle("MessageFailure|hello"));
	}

}
