package simcommon.messages.systemMessages;

import simcommon.entities.User;
import simcommon.messages.systemMessages.source.ServerMessageSource;
import simcommon.utilities.ParamUtil;

public class AuthenticationResponseSystemMessage extends SystemMessage {

	protected User mUser;
	// TODO: use the general failure message instead of this one to indicate
	// failure and have this one indicate just success?
	protected boolean mSuccessful;

	public AuthenticationResponseSystemMessage(User user) {
		super(new ServerMessageSource());
		ParamUtil.validateParamNotNull("successful authentication must have user", user);
		mUser = user;
		mSuccessful = true;
	}

	public AuthenticationResponseSystemMessage() {
		super(new ServerMessageSource());
		mUser = null;
		mSuccessful = false;
	}

	/**
	 * @return user, or null if failed
	 */
	public User getUser() {
		return mUser;
	}

	public boolean authenticationSuccessful() {
		return mSuccessful;
	}
}
