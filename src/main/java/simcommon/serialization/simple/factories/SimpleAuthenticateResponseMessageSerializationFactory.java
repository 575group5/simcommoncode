package simcommon.serialization.simple.factories;

import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.ISpecificMessageSerializerDeserializerFactory;
import simcommon.serialization.simple.deserializers.SimpleAuthenticateResponseMessageDeserializer;
import simcommon.serialization.simple.serializers.SimpleAuthenticateResponseMessageSerializer;

public class SimpleAuthenticateResponseMessageSerializationFactory implements ISpecificMessageSerializerDeserializerFactory {

	@Override
	public IIndividualMessageSerializer getSpecificSerializer() {
		return new SimpleAuthenticateResponseMessageSerializer();
	}

	@Override
	public IIndividualMessageDeserializer getSpecificDeserializer() {
		return new SimpleAuthenticateResponseMessageDeserializer();
	}

}
