package simcommon.serialization;

public interface ISpecificMessageSerializerDeserializerFactory {

	IIndividualMessageSerializer getSpecificSerializer();
	IIndividualMessageDeserializer getSpecificDeserializer();
}
