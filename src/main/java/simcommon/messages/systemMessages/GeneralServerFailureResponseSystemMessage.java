package simcommon.messages.systemMessages;

import simcommon.FailReason;
import simcommon.messages.systemMessages.source.ServerMessageSource;
import simcommon.utilities.ParamUtil;

public class GeneralServerFailureResponseSystemMessage extends SystemMessage {

	private FailReason mFailureReason;

	public GeneralServerFailureResponseSystemMessage(FailReason failReason) {
		super(new ServerMessageSource());
		ParamUtil.validateParamNotNull("must have failure reason", failReason);
		mFailureReason = failReason;
	}

	public FailReason getFailureReason() {
		return mFailureReason;
	}
}
