package simcommon.serialization;

import java.io.OutputStream;

import simcommon.messages.userMessages.IUserMessage;

public interface IUserMessageSerializer {

	boolean canHandle(IUserMessage message);
	void writeToOutput(IUserMessage message, OutputStream outputStream);
}
