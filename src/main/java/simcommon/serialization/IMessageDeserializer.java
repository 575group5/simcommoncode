package simcommon.serialization;

import simcommon.messages.systemMessages.SystemMessage;

public interface IMessageDeserializer {

	SystemMessage readFromInput(String input) throws DeserializationException;
}
