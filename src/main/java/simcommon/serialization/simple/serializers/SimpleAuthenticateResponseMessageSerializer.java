package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IIndividualMessageSerializer;

public class SimpleAuthenticateResponseMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer {

	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) {
		AuthenticationResponseSystemMessage arsm = (AuthenticationResponseSystemMessage) message;
		String sendMessage = "AuthResponse-authfail"; 
		if (arsm.authenticationSuccessful()) {
		sendMessage = "AuthResponse-username|" + arsm.getUser().getUserInfo().getName() + "|useridstrng|"
				+ arsm.getUser().getUserId().getUserId();
		} 
		this.printToStream(sendMessage, out);
	}

	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof AuthenticationResponseSystemMessage;
	}

}