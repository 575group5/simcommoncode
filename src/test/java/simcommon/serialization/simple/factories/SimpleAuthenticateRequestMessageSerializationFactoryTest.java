package simcommon.serialization.simple.factories;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.simple.deserializers.SimpleAuthenticateRequestMessageDeserializer;
import simcommon.serialization.simple.serializers.SimpleAuthenticateRequestMessageSerializer;

public class SimpleAuthenticateRequestMessageSerializationFactoryTest {
	SimpleAuthenticateRequestMessageSerializationFactory testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleAuthenticateRequestMessageSerializationFactory();
	}
	
	@Test
	public void testGetSpecificSerializer(){
		assertTrue(testObj.getSpecificSerializer() instanceof SimpleAuthenticateRequestMessageSerializer);
	}
	
	@Test
	public void testGetSpecificDeSerializer(){
		assertTrue(testObj.getSpecificDeserializer() instanceof SimpleAuthenticateRequestMessageDeserializer);
	}

}
