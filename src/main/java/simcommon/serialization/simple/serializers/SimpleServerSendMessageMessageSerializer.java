package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.ServerSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.PlainAsciiUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.messages.userMessages.origin.ServerOrigin;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.SerializationException;

public class SimpleServerSendMessageMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer {

	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof ServerSendMessageRequestSystemMessage &&
				((ServerSendMessageRequestSystemMessage)message).getMessage() instanceof PlainAsciiUserMessage;
	}

	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) throws SerializationException {
		Gson gson = new GsonBuilder().create();
		String origin = getOrigin(((ServerSendMessageRequestSystemMessage)message).getSource());
		
		JSONObject json = new JSONObject(gson.toJson(message));
		json.put("mOrigin", origin);
		
		StringBuilder sb = new StringBuilder();
		sb.append("ServerSendMessage|");
		sb.append(json.toString());
		this.printToStream(sb.toString(),out);
	}

	private String getOrigin(IUserMessageOrigin source) {
		String originVal = "userOrigin";
		if(source instanceof ServerOrigin){
			originVal = "serverOrigin";
		}
		return originVal;
	}

}
