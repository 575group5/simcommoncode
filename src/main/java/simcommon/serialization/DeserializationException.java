package simcommon.serialization;

public class DeserializationException extends Exception {

	private static final long serialVersionUID = 5713877171286141001L;

	public DeserializationException(String message) {
		super(message);
	}

	public DeserializationException(String message, Exception e) {
		super(message, e);
	}
}
