package simcommon.serialization.simple.factories;

import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.ISpecificMessageSerializerDeserializerFactory;
import simcommon.serialization.simple.deserializers.SimpleDirectoryResponseMessageDeserializer;
import simcommon.serialization.simple.serializers.SimpleDirectoryResponseMessageSerializer;

public class SimpleDirectoryResponseMessageSerializationFactory implements ISpecificMessageSerializerDeserializerFactory {

	@Override
	public IIndividualMessageSerializer getSpecificSerializer() {
		return new SimpleDirectoryResponseMessageSerializer();
	}

	@Override
	public IIndividualMessageDeserializer getSpecificDeserializer() {
		return new SimpleDirectoryResponseMessageDeserializer();
	}

}
