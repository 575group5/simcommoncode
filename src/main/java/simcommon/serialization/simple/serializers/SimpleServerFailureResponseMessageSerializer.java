package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.systemMessages.source.ServerMessageSource;
import simcommon.messages.systemMessages.source.UserMessageSource;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.IUserSerializer;
import simcommon.serialization.SerializationException;

public class SimpleServerFailureResponseMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer{

	protected IUserSerializer mUserSerializer;
	
	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof GeneralServerFailureResponseSystemMessage;
	}

	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) throws SerializationException {
		String sendMessage = "MessageFailure|" + ((ServerMessageSource) message.getMessageSource());
		this.printToStream(sendMessage, out);
	}
}
