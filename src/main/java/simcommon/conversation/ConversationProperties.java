package simcommon.conversation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import simcommon.entities.User;
import simcommon.utilities.ParamUtil;

public class ConversationProperties {
	private Collection<User> mParticipants;
	private String mName;
	private boolean mIsPublic;

	public ConversationProperties(Collection<User> participants, String name, boolean isPublic) {
		ParamUtil.validateParamNotNull("must have participants", participants);
		mParticipants = new ArrayList<>(participants);
		mName = name;
		mIsPublic = isPublic;
	}

	public boolean isPublic() {
		return mIsPublic;
	}

	public String getName() {
		return mName;
	}

	public Collection<User> getParticipants() {
		return Collections.unmodifiableCollection(mParticipants);
	}

	@Override
	public int hashCode() {
		// generated
		final int prime = 31;
		int result = 1;
		result = prime * result + (mIsPublic ? 1231 : 1237);
		result = prime * result + ((mName == null) ? 0 : mName.hashCode());
		result = prime * result + ((mParticipants == null) ? 0 : mParticipants.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		// generated
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConversationProperties other = (ConversationProperties) obj;
		if (mIsPublic != other.mIsPublic)
			return false;
		if (mName == null) {
			if (other.mName != null)
				return false;
		} else if (!mName.equals(other.mName))
			return false;
		if (mParticipants == null) {
			if (other.mParticipants != null)
				return false;
		} else if (!mParticipants.equals(other.mParticipants))
			return false;
		return true;
	}

}
