package simcommon.entities;

public class UserInfo {

	private String mName;

	public UserInfo(final String username) {
		mName = username;
	}

	public String getName() {
		return mName;
	}
}
