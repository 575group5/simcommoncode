package simcommon.serialization;

import java.io.InputStream;

import simcommon.conversation.Conversation;

public interface IConversationDeserializer {

	Conversation readFromInput(InputStream inputStream);
}
