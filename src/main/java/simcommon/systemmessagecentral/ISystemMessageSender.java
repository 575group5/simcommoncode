package simcommon.systemmessagecentral;

import simcommon.messages.systemMessages.SystemMessage;

public interface ISystemMessageSender {
	/**
	 * @param callback
	 *            - callback for status of this message, can be null
	 */
	boolean sendMessage(SystemMessage message, IStatusCallback callback);
}
