package simcommon.serialization.simple.serializers;

import static org.junit.Assert.assertTrue;

import java.io.OutputStream;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;

public class SimpleDirectoryResponseMessageSerializerTest {
	SimpleDirectoryResponseMessageSerializer testObj;

	@Before
	public void setup() {
		testObj = new SimpleDirectoryResponseMessageSerializer();
	}

	@Test
	public void testWriteToOutput(){
		SystemMessage message = new DirectoryResponseSystemMessage(new ArrayList<>());
		OutputStream out = Mockito.mock(OutputStream.class);
		testObj.writeToOutput(message, out);
	}

	@Test
	public void testCanHandle() {
		assertTrue(testObj.canHandle(new DirectoryResponseSystemMessage(new ArrayList<>())));
	}

}
