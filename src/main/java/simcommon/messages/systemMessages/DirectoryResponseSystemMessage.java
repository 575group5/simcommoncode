package simcommon.messages.systemMessages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import simcommon.conversation.Conversation;
import simcommon.messages.systemMessages.source.ServerMessageSource;
import simcommon.utilities.ParamUtil;

public class DirectoryResponseSystemMessage extends SystemMessage {

	private final Collection<Conversation> mConversations;
	
	public DirectoryResponseSystemMessage(Collection<Conversation> conversations) {
		super(new ServerMessageSource());
		ParamUtil.validateParamNotNull("conversation collection cannot be null", conversations);
		mConversations = Collections.unmodifiableCollection(new ArrayList<>(conversations));
	}

	public Collection<Conversation> getConversations() {
		return mConversations;
	}
}
