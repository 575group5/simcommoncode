package simcommon.serialization.simple.factories;

import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.ISpecificMessageSerializerDeserializerFactory;
import simcommon.serialization.simple.deserializers.SimpleDisconnectMessageDeserializer;
import simcommon.serialization.simple.serializers.SimpleDisconnectMessageSerializer;

public class SimpleDisconnectMessageSerializationFactory implements ISpecificMessageSerializerDeserializerFactory {

	@Override
	public IIndividualMessageSerializer getSpecificSerializer() {
		return new SimpleDisconnectMessageSerializer();
	}

	@Override
	public IIndividualMessageDeserializer getSpecificDeserializer() {
		return new SimpleDisconnectMessageDeserializer();
	}

}
