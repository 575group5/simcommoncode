package simcommon.identification;

public interface IServerIdentityObtainedCallback {

	void onServerIdentityObtained(IServerIdentity identity);
}
