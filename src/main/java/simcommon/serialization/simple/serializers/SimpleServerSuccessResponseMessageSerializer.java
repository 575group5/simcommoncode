package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import simcommon.messages.systemMessages.GeneralServerSuccessResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.systemMessages.source.ServerMessageSource;
import simcommon.messages.systemMessages.source.UserMessageSource;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.IUserSerializer;
import simcommon.serialization.SerializationException;

public class SimpleServerSuccessResponseMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer {

	protected IUserSerializer mUserSerializer;
	
	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof GeneralServerSuccessResponseSystemMessage;
	}

	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) throws SerializationException {
		String sendMessage = "MessageSuccess";
		this.printToStream(sendMessage, out);
	}

}
