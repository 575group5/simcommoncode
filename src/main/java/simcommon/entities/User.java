package simcommon.entities;

import simcommon.utilities.ParamUtil;

public class User{
	private final UserInfo mUserInfo;

	private final UserId mUserId;

	public User(UserInfo info, UserId id) {
		ParamUtil.validateParamNotNull("UserInfo is required", info);
		ParamUtil.validateParamNotNull("UserId is required", id);
		mUserInfo = info;
		mUserId = id;
	}

	public UserInfo getUserInfo() {
		return mUserInfo;
	}

	public UserId getUserId() {
		return mUserId;
	}

	@Override
	// generated
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mUserId == null) ? 0 : mUserId.hashCode());
		return result;
	}

	@Override
	// generated
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (mUserId == null) {
			if (other.mUserId != null)
				return false;
		} else if (!mUserId.equals(other.mUserId))
			return false;
		return true;
	}
}