package simcommon.serialization.simple.deserializers;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;

public class SimpleDirectoryRequestMessageDeserializerTest {
	SimpleDirectoryRequestMessageDeserializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleDirectoryRequestMessageDeserializer();
	}
	
	@Test
	public void testReadFromInput(){
		SystemMessage inputMessage = testObj.readFromInput("dirReq-user:hello");
		assertTrue(inputMessage instanceof DirectoryRequestSystemMessage);
	}
	
	@Test
	public void testCanHandle(){
		assertTrue(testObj.canHandle("dirReq-user:|hello"));
	}

}
