package simcommon.serialization.simple.deserializers;

import static org.junit.Assert.assertTrue;

import java.io.OutputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import simcommon.messages.systemMessages.GeneralServerSuccessResponseSystemMessage;
import simcommon.serialization.SerializationException;
import simcommon.serialization.simple.serializers.SimpleServerSuccessResponseMessageSerializer;

public class SimpleServerSuccessResponseMessageSerializerTest {
	SimpleServerSuccessResponseMessageSerializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleServerSuccessResponseMessageSerializer();
	}
	
	@Test
	public void testReadFromInput() throws SerializationException{
		OutputStream out = Mockito.mock(OutputStream.class);
		testObj.writeToOutput(new GeneralServerSuccessResponseSystemMessage(), out);
	}
	
	@Test
	public void testCanHandle(){
		assertTrue(testObj.canHandle(new GeneralServerSuccessResponseSystemMessage()));
	}

}
