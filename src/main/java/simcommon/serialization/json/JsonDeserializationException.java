package simcommon.serialization.json;

public class JsonDeserializationException extends Exception {
	public JsonDeserializationException(String reason) {
		super(reason);
	}

	public JsonDeserializationException(String reason, Throwable cause) {
		super(reason, cause);
	}
}
