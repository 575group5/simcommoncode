package simcommon.utilities;

public class ParamUtil {
	public static void validateParamNotNull(String message, Object param) {
		validateConditionTrue(message, param != null);
	}

	public static void validateConditionFalse(String message, boolean expression) {
		validateConditionTrue(message, !expression);
	}

	public static void validateConditionTrue(String message, boolean expression) {
		if (!expression) {
			throw new IllegalArgumentException(message);
		}
	}
}
