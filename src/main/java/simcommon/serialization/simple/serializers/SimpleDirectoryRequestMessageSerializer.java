package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.systemMessages.source.UserMessageSource;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.IUserSerializer;
import simcommon.utilities.ParamUtil;

public class SimpleDirectoryRequestMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer {

	protected IUserSerializer mUserSerializer;
	
	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) {
		ParamUtil.validateConditionTrue("can't handle this message", canHandle(message));
		String sendMessage = "dirReq-user:" + ((UserMessageSource) ((DirectoryRequestSystemMessage) message).getMessageSource())
				.getUser().getUserId().getUserId();
		this.printToStream(sendMessage, out);
	}

	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof DirectoryRequestSystemMessage;
	}

}
