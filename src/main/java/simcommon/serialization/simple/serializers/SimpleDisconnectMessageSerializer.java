package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.systemMessages.source.UserMessageSource;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.IUserSerializer;

public class SimpleDisconnectMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer {

	protected IUserSerializer mUserSerializer;

	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) {
		String sendMessage = "ClientDis-user|" + ((UserMessageSource) message.getMessageSource()).getUser().getUserId().getUserId();
		this.printToStream(sendMessage, out);
	}

	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof ClientDisconnectRequestSystemMessage;
	}

}
