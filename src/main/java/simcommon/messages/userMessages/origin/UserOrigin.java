package simcommon.messages.userMessages.origin;

import simcommon.entities.User;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.utilities.ParamUtil;

/**
 * Represents the origin of a {@link IUserMessage} when it comes from another
 * User.
 */
public class UserOrigin implements IUserMessageOrigin {
	private final User mUser;

	public UserOrigin(User origin) {
		ParamUtil.validateParamNotNull("must have user", origin);
		mUser = origin;
	}

	public User getOriginUser() {
		return mUser;
	}

	@Override
	// generated
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mUser == null) ? 0 : mUser.hashCode());
		return result;
	}

	@Override
	// generated
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserOrigin other = (UserOrigin) obj;
		if (mUser == null) {
			if (other.mUser != null)
				return false;
		} else if (!mUser.equals(other.mUser))
			return false;
		return true;
	}

}
