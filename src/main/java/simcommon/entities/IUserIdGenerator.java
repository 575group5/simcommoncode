package simcommon.entities;

public interface IUserIdGenerator {

	UserId generateUserId();
}
