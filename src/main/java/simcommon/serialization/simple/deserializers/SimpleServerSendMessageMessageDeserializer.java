package simcommon.serialization.simple.deserializers;

import org.json.JSONException;
import org.json.JSONObject;

import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.ServerSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.messages.userMessages.origin.ServerOrigin;
import simcommon.messages.userMessages.origin.UserOrigin;
import simcommon.serialization.DeserializationException;
import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.json.ConversationJsonSerializerDeserializer;
import simcommon.serialization.json.JsonDeserializationException;
import simcommon.serialization.json.UserJsonSerializerDeserializer;
import simcommon.serialization.json.UserMessageDeserializer;

public class SimpleServerSendMessageMessageDeserializer implements IIndividualMessageDeserializer {

	@Override
	public SystemMessage readFromInput(String input) throws DeserializationException {
		int jsonStart = input.indexOf("|");
		String json = input.substring(jsonStart + 1);
		JSONObject jsonMessage = new JSONObject(json);
		ServerSendMessageRequestSystemMessage message = null;
		try {
			Conversation conversation = ConversationJsonSerializerDeserializer.fromJson(jsonMessage.getJSONObject("mConversation"));
			IUserMessage userMessage = UserMessageDeserializer.fromJSON(jsonMessage.getJSONObject("mMessage"));
			IUserMessageOrigin origin = getOrigin(jsonMessage);
			message = new ServerSendMessageRequestSystemMessage(origin, conversation, userMessage);
		} catch (JSONException | JsonDeserializationException e) {
			throw new DeserializationException(e.getMessage());
		}
		return message;
	}
	
	private IUserMessageOrigin getOrigin(JSONObject jsonMessage) throws JSONException, JsonDeserializationException {
		String source = (String)jsonMessage.get("mOrigin");
		IUserMessageOrigin origin = new ServerOrigin();
		if(source.equals("userOrigin")){
			JSONObject jsonOrigin = jsonMessage.getJSONObject("mMessageSource");
			User user = UserJsonSerializerDeserializer.fromJson(jsonOrigin.getJSONObject("mUser"));
			origin = new UserOrigin(user);
		}
		return origin;
	}
	
	@Override
	public boolean canHandle(String input) {
		return input.startsWith("ServerSendMessage");
	}
}
