# SimCommonCode

SimCommonCode is a shared library used by both the SimServer and SimClient components. 

## Installation

The project utilizes Apache Maven(https://maven.apache.org/) for dependency management and to build an executable jar.
For maven integration with eclipse a minimum version of Mars 4.5.1 is recommended.
After cloning the repository and importing the project into eclipse executing the following command will produce a jar:

mvn clean build

If eclipse is being used jars can also be produced by utilizing the interface and doing the following:
Right click the project -> Run As -> Maven Install

## History

1.0 First release

## Credits
Ken Balogh, Dan Ford, Greg Warchol, Lindsey Young