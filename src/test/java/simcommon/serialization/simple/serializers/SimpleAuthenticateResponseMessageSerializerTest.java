package simcommon.serialization.simple.serializers;

import static org.junit.Assert.assertTrue;

import java.io.OutputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;

public class SimpleAuthenticateResponseMessageSerializerTest {
	SimpleAuthenticateResponseMessageSerializer testObj;

	@Before
	public void setup() {
		testObj = new SimpleAuthenticateResponseMessageSerializer();
	}

	@Test
	public void testWriteToOutput(){
		AuthenticationResponseSystemMessage message = new AuthenticationResponseSystemMessage();
		OutputStream out = Mockito.mock(OutputStream.class);
		testObj.writeToOutput(message, out);
	}

	@Test
	public void testCanHandle() {
		assertTrue(testObj.canHandle(new AuthenticationResponseSystemMessage()));
	}

}
