package simcommon.conversation;

public interface IConversationIdentifier {

	String getConversationIdentifier(Conversation conversation);
}
