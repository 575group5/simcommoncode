package simcommon.serialization;

import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationId;

public interface IConversationHydrationStrategy {

	Conversation getConversation(ConversationId conversationId);
}
