package simcommon.serialization.json;

import org.json.JSONObject;

import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.PlainAsciiUserMessage;

public class UserMessageDeserializer {

	public static IUserMessage fromJSON(JSONObject jsonUserMessage) {
		String content = (String) jsonUserMessage.get("mContent");
		PlainAsciiUserMessage userMessage = new PlainAsciiUserMessage(content);
		return userMessage;
	}
}
