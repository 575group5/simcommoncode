package simcommon.messages.userMessages.origin;

public interface IUserMessageOriginIdentifier {

	String getOriginIdentifier(IUserMessageOrigin origin);
}
