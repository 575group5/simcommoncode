package simcommon.serialization.simple.deserializers;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;

public class SimpleAuthenticateResponseMessageDeserializerTest {
	SimpleAuthenticateResponseMessageDeserializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleAuthenticateResponseMessageDeserializer();
	}
	
	@Test
	public void testReadFromInput(){
		SystemMessage inputMessage = testObj.readFromInput("AuthResponse-authfail|hello");
		assertTrue(inputMessage instanceof AuthenticationResponseSystemMessage);
	}
	
	@Test
	public void testReadFromInput_Other(){
		SystemMessage inputMessage = testObj.readFromInput("other|username|hellothisisatest");
		assertTrue(inputMessage instanceof AuthenticationResponseSystemMessage);
	}
	
	@Test
	public void testCanHandle(){
		assertTrue(testObj.canHandle("AuthResponse-|hello"));
	}
}
