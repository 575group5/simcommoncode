package simcommon.serialization;

public class SerializationException extends Exception {
	private static final long serialVersionUID = -4803464549738886253L;

	public SerializationException(String message) {
		super(message);
	}

	public SerializationException(String message, Exception e) {
		super(message, e);
	}
}
