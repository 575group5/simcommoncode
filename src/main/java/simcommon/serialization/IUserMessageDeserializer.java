package simcommon.serialization;

import java.io.InputStream;

import simcommon.messages.userMessages.IUserMessage;

public interface IUserMessageDeserializer {

	boolean canHandle(InputStream inputStream);
	IUserMessage readFromInputStream(InputStream inputStream);
}
