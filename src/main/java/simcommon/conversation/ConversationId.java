package simcommon.conversation;

public class ConversationId {

	private final long mId;
	
	public ConversationId(long id) {
		mId = id;
	}

	public long getLongId() {
		return mId;
	}

	@Override
	// generated
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (mId ^ (mId >>> 32));
		return result;
	}

	@Override
	// generated
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConversationId other = (ConversationId) obj;
		if (mId != other.mId)
			return false;
		return true;
	}

	public String asString(){
		return String.valueOf(mId);
	}
}
