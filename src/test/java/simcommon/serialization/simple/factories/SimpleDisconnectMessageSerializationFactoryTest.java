package simcommon.serialization.simple.factories;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.simple.deserializers.SimpleAuthenticateRequestMessageDeserializer;
import simcommon.serialization.simple.deserializers.SimpleAuthenticateResponseMessageDeserializer;
import simcommon.serialization.simple.deserializers.SimpleClientSendMessageMessageDeserializer;
import simcommon.serialization.simple.deserializers.SimpleDirectoryRequestMessageDeserializer;
import simcommon.serialization.simple.deserializers.SimpleDirectoryResponseMessageDeserializer;
import simcommon.serialization.simple.deserializers.SimpleDisconnectMessageDeserializer;
import simcommon.serialization.simple.serializers.SimpleAuthenticateRequestMessageSerializer;
import simcommon.serialization.simple.serializers.SimpleAuthenticateResponseMessageSerializer;
import simcommon.serialization.simple.serializers.SimpleClientSendMessageMessageSerializer;
import simcommon.serialization.simple.serializers.SimpleDirectoryRequestMessageSerializer;
import simcommon.serialization.simple.serializers.SimpleDirectoryResponseMessageSerializer;
import simcommon.serialization.simple.serializers.SimpleDisconnectMessageSerializer;

public class SimpleDisconnectMessageSerializationFactoryTest {
	SimpleDisconnectMessageSerializationFactory testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleDisconnectMessageSerializationFactory();
	}
	
	@Test
	public void testGetSpecificSerializer(){
		assertTrue(testObj.getSpecificSerializer() instanceof SimpleDisconnectMessageSerializer);
	}
	
	@Test
	public void testGetSpecificDeSerializer(){
		assertTrue(testObj.getSpecificDeserializer() instanceof SimpleDisconnectMessageDeserializer);
	}

}
