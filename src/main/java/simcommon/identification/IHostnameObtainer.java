package simcommon.identification;

public interface IHostnameObtainer {
	
	void obtainHostname(IHostnameObtainedCallback callback);
}
