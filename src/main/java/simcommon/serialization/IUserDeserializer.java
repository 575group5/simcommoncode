package simcommon.serialization;

import java.io.InputStream;

import simcommon.entities.User;

public interface IUserDeserializer {

	User readFromInput(InputStream inputStream);
}
