package simcommon.conversation;

import java.util.concurrent.atomic.AtomicLong;

public class InMemoryConversationIdGenerator implements ConversationIdGenerator {
	private final AtomicLong mConversationIdGenerator = new AtomicLong(0);

	@Override
	public ConversationId generateConversationId() {
		return new ConversationId(mConversationIdGenerator.getAndIncrement());
	}

}
