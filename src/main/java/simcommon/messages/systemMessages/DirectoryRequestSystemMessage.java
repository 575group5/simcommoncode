package simcommon.messages.systemMessages;

import simcommon.entities.User;
import simcommon.messages.systemMessages.source.UserMessageSource;

public class DirectoryRequestSystemMessage extends SystemMessage {

	public DirectoryRequestSystemMessage(User source) {
		super(new UserMessageSource(source));
	}

	public User getSource() {
		return ((UserMessageSource) getMessageSource()).getUser();
	}
}
