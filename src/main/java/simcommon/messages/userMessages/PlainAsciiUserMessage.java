package simcommon.messages.userMessages;

public class PlainAsciiUserMessage implements IUserMessage {

	private String mContent;
	
	public PlainAsciiUserMessage(String content){
		mContent = content;
	}
	
	public String getContent(){
		return mContent;
	}
}
