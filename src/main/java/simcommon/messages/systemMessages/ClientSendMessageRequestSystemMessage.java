package simcommon.messages.systemMessages;

import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.source.UserMessageSource;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.UserOrigin;
import simcommon.utilities.ParamUtil;

public class ClientSendMessageRequestSystemMessage extends SystemMessage {

	protected Conversation mConversation;
	protected IUserMessage mMessage;
	
	public ClientSendMessageRequestSystemMessage(User source, Conversation conversation, IUserMessage message) {
		super(new UserMessageSource(source));
		ParamUtil.validateParamNotNull("must have user source", source);
		ParamUtil.validateParamNotNull("must have conversation", conversation);
		ParamUtil.validateParamNotNull("must have message", message);
		mConversation = conversation;
		mMessage = message;
	}
	
	public Conversation getConversation(){
		return mConversation;
	}
	
	public IUserMessage getUserMessage(){
		return mMessage;
	}

	public UserOrigin getMessageOrigin() {
		return new UserOrigin(((UserMessageSource) getMessageSource()).getUser());
	}
}
