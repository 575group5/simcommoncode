package simcommon.entities;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import simcommon.entities.InMemoryUserIdGenerator;
import simcommon.entities.UserId;

@RunWith(MockitoJUnitRunner.class)
public class InMemoryUserIdGeneratorTest {
	@InjectMocks
	InMemoryUserIdGenerator testObj;
	
	@Test
	public void testUserIdGenerated(){
		assertNotNull(testObj.generateUserId());
	}
	
	@Test
	public void testIdGenerated(){
		UserId id = testObj.generateUserId();
		assertNotNull(id.getUserId());
	}
	
	@Test
	public void testIdUniqueEnough(){
		List<String> allIds = new ArrayList<>();
		IntStream.range(0, 100).forEach(i -> {
			UserId userId = testObj.generateUserId();
			String id = userId.getUserId();
			assertFalse(allIds.contains(id));
			allIds.add(id);
		});
	}
}
