package simcommon.conversation;

public interface ConversationIdGenerator {
	ConversationId generateConversationId();
}
