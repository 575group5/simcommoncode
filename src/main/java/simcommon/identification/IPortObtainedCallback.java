package simcommon.identification;

public interface IPortObtainedCallback {
	
	void onPortObtained(int port);
}
