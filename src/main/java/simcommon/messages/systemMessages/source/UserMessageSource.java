package simcommon.messages.systemMessages.source;

import simcommon.entities.User;
import simcommon.utilities.ParamUtil;

public class UserMessageSource implements IMessageSource {

	private User mUser;

	public UserMessageSource(User user) {
		ParamUtil.validateParamNotNull("User Message Soruce must have user", user);
		mUser = user;
	}

	public User getUser() {
		return mUser;
	}
}
