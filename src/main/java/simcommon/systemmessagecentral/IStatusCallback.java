package simcommon.systemmessagecentral;

public interface IStatusCallback {
	void onSuccess();

	void onFailure();
}
