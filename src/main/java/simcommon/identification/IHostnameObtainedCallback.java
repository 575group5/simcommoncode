package simcommon.identification;

public interface IHostnameObtainedCallback {

	void onHostnameObtained(String hostname);
}
