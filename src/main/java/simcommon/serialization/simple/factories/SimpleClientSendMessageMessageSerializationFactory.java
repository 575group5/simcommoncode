package simcommon.serialization.simple.factories;

import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.ISpecificMessageSerializerDeserializerFactory;
import simcommon.serialization.simple.deserializers.SimpleClientSendMessageMessageDeserializer;
import simcommon.serialization.simple.serializers.SimpleClientSendMessageMessageSerializer;

public class SimpleClientSendMessageMessageSerializationFactory implements ISpecificMessageSerializerDeserializerFactory {

	@Override
	public IIndividualMessageSerializer getSpecificSerializer() {
		return new SimpleClientSendMessageMessageSerializer();
	}

	@Override
	public IIndividualMessageDeserializer getSpecificDeserializer() {
		return new SimpleClientSendMessageMessageDeserializer();
	}

}
