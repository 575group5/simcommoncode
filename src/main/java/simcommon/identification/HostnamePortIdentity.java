package simcommon.identification;

public class HostnamePortIdentity implements IServerIdentity {

	private String mHostname;
	private int mPort;

	public HostnamePortIdentity(String hostname, int port) {
		mHostname = hostname;
		mPort = port;
	}

	public String getHostname() {
		return mHostname;
	}

	public int getPort() {
		return mPort;
	}

}
