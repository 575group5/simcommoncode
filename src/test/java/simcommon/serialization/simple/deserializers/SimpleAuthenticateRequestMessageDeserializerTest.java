package simcommon.serialization.simple.deserializers;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;

public class SimpleAuthenticateRequestMessageDeserializerTest {
	SimpleAuthenticateRequestMessageDeserializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleAuthenticateRequestMessageDeserializer();
	}
	
	@Test
	public void testReadFromInput(){
		SystemMessage inputMessage = testObj.readFromInput("AuthReq-name|hello");
		assertTrue(inputMessage instanceof AuthenticationRequestSystemMessage);
	}
	
	@Test
	public void testCanHandle(){
		assertTrue(testObj.canHandle("AuthReq-name|hello"));
	}

}
