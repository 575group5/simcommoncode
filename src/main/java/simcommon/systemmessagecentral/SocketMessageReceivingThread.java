package simcommon.systemmessagecentral;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.DeserializationException;
import simcommon.serialization.IMessageDeserializer;


public class SocketMessageReceivingThread extends Thread {
	private ISystemMessageReceivedListener mListener;
	IMessageDeserializer des;
	PrintWriter out;
	BufferedReader in;

	public SocketMessageReceivingThread(PrintWriter out, BufferedReader in, IMessageDeserializer des,
			ISystemMessageReceivedListener mListener) {
		this.in = in;
		this.out = out;
		this.des = des;
		this.mListener = mListener;
	}

	@Override
	public void run() {
		try {
			String fromSocket;
			while (true) {
				fromSocket = in.readLine();
				SystemMessage mess = des.readFromInput(fromSocket);
				mListener.onMessageReceived(mess);
			}
		} catch (IOException e) {
			 e.printStackTrace();
		} catch (DeserializationException e) {
			 e.printStackTrace();
		}
	}
}
