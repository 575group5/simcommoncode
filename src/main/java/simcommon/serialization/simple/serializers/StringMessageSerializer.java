package simcommon.serialization.simple.serializers;

import java.io.OutputStream;
import java.io.PrintWriter;

public abstract class StringMessageSerializer {
	public void printToStream(String message, OutputStream out){
		PrintWriter writer = new PrintWriter(out, true);
		writer.println(message);
		writer.flush();
	}

}
