package simcommon.serialization;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.utilities.ParamUtil;

public class DelegatingMessageSerializer implements IMessageSerializer {

	private Collection<IIndividualMessageSerializer> mSerializers;

	public static class Builder {
		private Collection<IIndividualMessageSerializer> mSerializers = new ArrayList<>();

		public Builder add(IIndividualMessageSerializer serializer) {
			ParamUtil.validateParamNotNull("serializer cannot be null", serializer);
			mSerializers.add(serializer);
			return this;
		}

		public DelegatingMessageSerializer build() {
			return new DelegatingMessageSerializer(this);
		}
	}

	private DelegatingMessageSerializer(Builder builder) {
		mSerializers = Collections.unmodifiableCollection(new ArrayList<>(builder.mSerializers));
	}
	
	@Override
	public void writeToOutput(SystemMessage systemMessage, OutputStream out) throws SerializationException {
		ParamUtil.validateParamNotNull("must have message", systemMessage);
		boolean handled = false;
		for (IIndividualMessageSerializer serializer : mSerializers) {
			if (serializer.canHandle(systemMessage)) {
				serializer.writeToOutput(systemMessage, out);
				handled = true;
			}
		}
		if(!handled){
			throw new SerializationException("none of our seriallizers handled message: " + systemMessage);
		}
	}
}

