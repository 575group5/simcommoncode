package simcommon.messages.systemMessages;

import simcommon.messages.systemMessages.source.IMessageSource;
import simcommon.utilities.ParamUtil;

public abstract class SystemMessage{

	protected IMessageSource mSource;

	public SystemMessage(IMessageSource source) {
		ParamUtil.validateParamNotNull("must have message source", source);
		mSource = source;
	}

	public IMessageSource getMessageSource() {
		return mSource;
	}
}
