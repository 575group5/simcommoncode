package simcommon.serialization.simple.deserializers;

import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IIndividualMessageDeserializer;

public class SimpleAuthenticateResponseMessageDeserializer implements IIndividualMessageDeserializer {

	@Override
	public SystemMessage readFromInput(String input) {
		// System.out.println(input);
		if (input.startsWith("AuthResponse-authfail")) {
			return new AuthenticationResponseSystemMessage();
		} else {
			int usernamestart = input.indexOf("|");
			int userNameend = input.indexOf("|", usernamestart + 1);
			int useridstart = input.indexOf("|", userNameend + 1);
			String userName = input.substring(usernamestart + 1, userNameend);
			String userId = input.substring(useridstart + 1);
			// System.out.println("deserialized auth response, user name = " +
			// userName + " user id = " + userId);
			return new AuthenticationResponseSystemMessage(new User(new UserInfo(userName), new UserId(userId)));
		}
	}

	@Override
	public boolean canHandle(String input) {
		return input.startsWith("AuthResponse-");
	}

}
