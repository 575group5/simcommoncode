package simcommon.identification;

public interface IServerIdentityObtainer {

	void obtainServerIdentity(IServerIdentityObtainedCallback callback);
}
