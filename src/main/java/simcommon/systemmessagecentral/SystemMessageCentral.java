package simcommon.systemmessagecentral;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simcommon.utilities.ParamUtil;

public abstract class SystemMessageCentral implements ISystemMessageSender {

	protected IMessageSerializer mSerializer;
	protected IMessageDeserializer mDeserializer;
	protected Collection<ISystemMessageReceivedListener> mListeners = new CopyOnWriteArrayList<>();

	public SystemMessageCentral(IMessageSerializer serializer, IMessageDeserializer deserializer) {
		ParamUtil.validateParamNotNull("must have serializer", serializer);
		ParamUtil.validateParamNotNull("must have deserializer", deserializer);
		mSerializer = serializer;
		mDeserializer = deserializer;
	}

	public void attachSystemMessageReceivedListener(ISystemMessageReceivedListener listener) {
		ParamUtil.validateParamNotNull("cannot attach null listener", listener);
		mListeners.add(listener);
	}

	public abstract void startListeningForMessages();

	@Override
	public abstract boolean sendMessage(SystemMessage message, IStatusCallback callback);

	public abstract void shutDown();

	protected void notifyListeners(SystemMessage message) {
		for (ISystemMessageReceivedListener listener : mListeners) {
			listener.onMessageReceived(message);
		}
	}
}
