package simcommon.serialization.simple.deserializers;

import simcommon.authentication.UsernameAuthentication;
import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IIndividualMessageDeserializer;

public class SimpleAuthenticateRequestMessageDeserializer implements IIndividualMessageDeserializer {

	@Override
	public SystemMessage readFromInput(String input) {
		String rest = input.replace("AuthReq-name|", "");
		return new AuthenticationRequestSystemMessage(new UsernameAuthentication(rest));
	}

	@Override
	public boolean canHandle(String input) {
		return input.startsWith("AuthReq-name|");
	}
}
