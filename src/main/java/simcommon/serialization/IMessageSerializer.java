package simcommon.serialization;

import java.io.OutputStream;

import simcommon.messages.systemMessages.SystemMessage;

public interface IMessageSerializer {

	void writeToOutput(SystemMessage message, OutputStream out) throws SerializationException;
}
