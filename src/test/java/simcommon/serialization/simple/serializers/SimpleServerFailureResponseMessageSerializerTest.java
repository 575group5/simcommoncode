package simcommon.serialization.simple.serializers;

import static org.junit.Assert.assertTrue;

import java.io.OutputStream;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import simcommon.FailReason;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryResponseSystemMessage;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.SerializationException;

public class SimpleServerFailureResponseMessageSerializerTest {
	SimpleServerFailureResponseMessageSerializer testObj;

	@Before
	public void setup() {
		testObj = new SimpleServerFailureResponseMessageSerializer();
	}

	@Test
	public void testWriteToOutput() throws SerializationException{
		SystemMessage message = new GeneralServerFailureResponseSystemMessage(FailReason.MISMATCH_USER);
		OutputStream out = Mockito.mock(OutputStream.class);
		testObj.writeToOutput(message, out);
	}

	@Test
	public void testCanHandle() {
		assertTrue(testObj.canHandle(new GeneralServerFailureResponseSystemMessage(FailReason.MISMATCH_USER)));
	}

}
