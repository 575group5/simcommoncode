package simcommon.serialization.simple.deserializers;

import simcommon.FailReason;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IUserDeserializer;

public class SimpleServerFailureResponseMessageDeserializer implements IIndividualMessageDeserializer{

	protected IUserDeserializer mUserDeserializer;
	
	@Override
	public SystemMessage readFromInput(String input) {
		return new GeneralServerFailureResponseSystemMessage(FailReason.valueOf(input));
	}

	@Override
	public boolean canHandle(String input) {
		return input.startsWith("MessageFailure");
	}

}
