package simcommon.serialization.json;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;

public class UserJsonSerializerDeserializer {

	public static JSONObject toJson(User user) {
		Gson gson = new GsonBuilder().create();
		return new JSONObject(gson.toJson(user));
	}

	public static User fromJson(JSONObject json) throws JsonDeserializationException {
		String name = json.getJSONObject("mUserInfo").getString("mName");
		if (name.isEmpty())
			throw new JsonDeserializationException("invalid name");
		String id = json.getJSONObject("mUserId").getString("mId");
		if (id.isEmpty())
			throw new JsonDeserializationException("invalid id");
		return new User(new UserInfo(name), new UserId(id));
	}

}
