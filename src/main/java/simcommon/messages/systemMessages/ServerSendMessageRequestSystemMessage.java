package simcommon.messages.systemMessages;

import simcommon.conversation.Conversation;
import simcommon.messages.systemMessages.source.ServerMessageSource;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.utilities.ParamUtil;

public class ServerSendMessageRequestSystemMessage extends SystemMessage {

	private IUserMessageOrigin mMessageSource;
	private Conversation mConversation;
	private IUserMessage mMessage;

	public ServerSendMessageRequestSystemMessage(IUserMessageOrigin messageOrigin, Conversation conversation,
			IUserMessage message) {
		super(new ServerMessageSource());
		ParamUtil.validateParamNotNull("must have source", messageOrigin);
		ParamUtil.validateParamNotNull("must have conversation", conversation);
		ParamUtil.validateParamNotNull("must have message", message);
		mMessageSource = messageOrigin;
		mConversation = conversation;
		mMessage = message;
	}

	public IUserMessageOrigin getSource() {
		return mMessageSource;
	}

	public Conversation getConversation() {
		return mConversation;
	}

	public IUserMessage getMessage() {
		return mMessage;
	}
}
