package simcommon.serialization.simple.serializers;

import static org.junit.Assert.assertTrue;

import java.io.OutputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;

public class SimpleDirectoryRequestMessageSerializerTest {
	SimpleDirectoryRequestMessageSerializer testObj;

	@Before
	public void setup() {
		testObj = new SimpleDirectoryRequestMessageSerializer();
	}

	@Test
	public void testWriteToOutput(){
		SystemMessage message = new DirectoryRequestSystemMessage(new User(new UserInfo("test"), new UserId("test")));
		OutputStream out = Mockito.mock(OutputStream.class);
		testObj.writeToOutput(message, out);
	}

	@Test
	public void testCanHandle() {
		assertTrue(testObj.canHandle(new DirectoryRequestSystemMessage(new User(new UserInfo("test"), new UserId("test")))));
	}

}
