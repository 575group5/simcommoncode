package simcommon.systemmessagecentral;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import simcommon.identification.HostnamePortIdentity;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simcommon.serialization.SerializationException;
import simcommon.utilities.ParamUtil;

public class SocketBasedSystemMessageCentral extends SystemMessageCentral {

	private Socket mSocket;
	boolean listening = false;

	private PrintWriter mOut;
	private BufferedReader mIn;

	public SocketBasedSystemMessageCentral(IMessageSerializer serializer, IMessageDeserializer deserializer,
			HostnamePortIdentity hostnamePortIdentity) throws IOException {
		super(serializer, deserializer);
		ParamUtil.validateParamNotNull("must have hostname port identity", hostnamePortIdentity);
		mSocket = new Socket(hostnamePortIdentity.getHostname(), hostnamePortIdentity.getPort());

		mOut = new PrintWriter(mSocket.getOutputStream(), true);
		mIn = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
	}

	public SocketBasedSystemMessageCentral(IMessageSerializer serializer, IMessageDeserializer deserializer,
			Socket socket) throws IOException {
		super(serializer, deserializer);
		ParamUtil.validateParamNotNull("must have socket", socket);
		mSocket = socket;

		mOut = new PrintWriter(mSocket.getOutputStream(), true);
		mIn = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
	}

	@Override
	public boolean sendMessage(SystemMessage message, IStatusCallback callback) {
		boolean success = true;
		try {
			mSerializer.writeToOutput(message, mSocket.getOutputStream());
		} catch (IOException | SerializationException e) {
			e.printStackTrace();
			success = false;
		}

		if (callback != null) {
			if (success) {
				callback.onSuccess();
			} else {
				callback.onFailure();
			}
		}

		return success;
	}

	@Override
	public void shutDown() {
		try {
			mSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void startListeningForMessages() {
		if (!listening) {
			listening = true;
			new SocketMessageReceivingThread(mOut, mIn, mDeserializer, new ISystemMessageReceivedListener() {

				@Override
				public void onMessageReceived(SystemMessage message) {
					SocketBasedSystemMessageCentral.super.notifyListeners(message);
				}
			}).start();
		}
	}
}
