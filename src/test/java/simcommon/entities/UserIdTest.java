package simcommon.entities;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import simcommon.entities.UserId;

@RunWith(MockitoJUnitRunner.class)
public class UserIdTest {
	UserId testObj;
	
	@Before
	public void setup(){
		testObj = new UserId("123");
	}
	
	@Test
	public void testEquals_NotEqualsNull(){
		assertFalse(testObj.equals(null));
	}
	
	@Test
	public void testEquals_NotEquals(){
		UserId otherUserId = new UserId("456");
		assertFalse(testObj.equals(otherUserId));
	}
	
	@Test
	public void testEquals_Equals(){
		UserId otherUserId = new UserId("123");
		assertTrue(testObj.equals(otherUserId));
	}
}
