package simcommon.serialization.simple.deserializers;

import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IUserDeserializer;

public class SimpleDisconnectMessageDeserializer implements IIndividualMessageDeserializer {

	protected IUserDeserializer mUserDeserializer;
	
	@Override
	public SystemMessage readFromInput(String input) {
		String userId = input.replace("ClientDis-user|", "");
		return new ClientDisconnectRequestSystemMessage(new User(new UserInfo("doesn't matter"), new UserId(userId)));
	}

	@Override
	public boolean canHandle(String input) {
		return input.startsWith("ClientDis-user|");
	}

}
