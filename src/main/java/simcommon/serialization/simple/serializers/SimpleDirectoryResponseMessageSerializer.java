package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import org.json.JSONArray;
import org.json.JSONObject;

import simcommon.conversation.Conversation;
import simcommon.messages.systemMessages.DirectoryResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.json.ConversationJsonSerializerDeserializer;
import simcommon.utilities.ParamUtil;

public class SimpleDirectoryResponseMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer {

	public static final String UNIQUE_ID_FIELD = "DirectoryRespose-Simple";
	public static final String CONVERSATIONS_LIST_KEY = "conversationsList";

	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) {
		ParamUtil.validateConditionTrue("can't handle", canHandle(message));
		DirectoryResponseSystemMessage directoryResponseSystemMessage = (DirectoryResponseSystemMessage) message;

		JSONObject serialized = new JSONObject();
		// the deserializer will know to recognize our responses from this
		serialized.put(UNIQUE_ID_FIELD, true);

		JSONArray conversationsArray = new JSONArray();
		for (Conversation conversation : directoryResponseSystemMessage.getConversations()) {
			conversationsArray.put(ConversationJsonSerializerDeserializer.toJson(conversation));
		}
		serialized.put(CONVERSATIONS_LIST_KEY, conversationsArray);
		
		this.printToStream(serialized.toString(), out);
	}

	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof DirectoryResponseSystemMessage;
	}

}