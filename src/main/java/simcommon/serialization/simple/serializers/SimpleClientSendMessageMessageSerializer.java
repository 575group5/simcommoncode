package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.PlainAsciiUserMessage;
import simcommon.serialization.IConversationSerializer;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.IUserMessageSerializer;
import simcommon.serialization.IUserSerializer;
import simcommon.serialization.SerializationException;

public class SimpleClientSendMessageMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer {

	protected IUserSerializer mUserSerializer;
	protected IConversationSerializer mConversationSerializer;
	protected IUserMessageSerializer mUserMessageSerializer;

	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof ClientSendMessageRequestSystemMessage &&
				((ClientSendMessageRequestSystemMessage)message).getUserMessage() instanceof PlainAsciiUserMessage;
	}

	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) throws SerializationException {
		Gson gson = new GsonBuilder().create();
		StringBuilder sb = new StringBuilder();
		sb.append("ClientSendMessage|");
		sb.append(gson.toJson(message));
		this.printToStream(sb.toString(),out);
	}

}
