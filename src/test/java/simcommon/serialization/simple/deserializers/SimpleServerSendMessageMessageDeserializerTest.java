package simcommon.serialization.simple.deserializers;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.ServerSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.DeserializationException;
import simcommon.serialization.json.JsonDeserializationException;

public class SimpleServerSendMessageMessageDeserializerTest {
	SimpleServerSendMessageMessageDeserializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleServerSendMessageMessageDeserializer();
	}
	
	@Test
	public void testReadFromInput() throws DeserializationException{
		SystemMessage inputMessage = testObj.readFromInput("ServerSendMessage|{mConversation:{mConversationId:{mId:1},mConversationProperties:{mName:test,mIsPublic:true,mParticipants:[]}},mSource:{mUser:{mUserInfo:{mName:name},mUserId:{mId:test}}},mMessage:{mContent:content},mOrigin:userOrigin,mMessageSource:{mUser:{mUserInfo:{mName:name},mUserId:{mId:test}}}}");
		assertTrue(inputMessage instanceof ServerSendMessageRequestSystemMessage);
	}
	
	@Test(expected=DeserializationException.class)
	public void testReadFromInput_Bad() throws DeserializationException{
		testObj.readFromInput("ServerSendMessage|{mConversation:{mConversationId:{mId:1},mConversationProperties:{mName:test,mIsPublic:true,mParticipants:[]}},mSource:{mUser:{mUserInfo:{mName:name},mUserId:{mId:test}}},mMessage:{mContent:content},mOrigin:userOrigin}");
	}
	
	@Test
	public void testCanHandle(){
		assertTrue(testObj.canHandle("ServerSendMessage"));
	}
}
