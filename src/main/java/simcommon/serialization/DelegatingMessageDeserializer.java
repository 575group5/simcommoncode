package simcommon.serialization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.utilities.ParamUtil;

public class DelegatingMessageDeserializer implements IMessageDeserializer {

	private Collection<IIndividualMessageDeserializer> mDeserializers;

	public static class Builder {
		private Collection<IIndividualMessageDeserializer> mDeserializers = new ArrayList<>();

		public Builder add(IIndividualMessageDeserializer deserializer) {
			ParamUtil.validateParamNotNull("deserializer cannot be null", deserializer);
			mDeserializers.add(deserializer);
			return this;
		}

		public DelegatingMessageDeserializer build() {
			return new DelegatingMessageDeserializer(this);
		}
	}

	private DelegatingMessageDeserializer(Builder builder) {
		mDeserializers = Collections.unmodifiableCollection(new ArrayList<>(builder.mDeserializers));
	}
	
	@Override
	public SystemMessage readFromInput(String input) throws DeserializationException {
		ParamUtil.validateParamNotNull("must have input", input);
		for (IIndividualMessageDeserializer deserializer : mDeserializers) {
			if (deserializer.canHandle(input)) {
				return deserializer.readFromInput(input);
			}
		}
		throw new DeserializationException("none of our deserialisers could handle input: " + input);
	}
}
