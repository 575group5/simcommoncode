package simcommon.messages.systemMessages;

import simcommon.messages.systemMessages.source.ServerMessageSource;

public class GeneralServerSuccessResponseSystemMessage extends SystemMessage {

	public GeneralServerSuccessResponseSystemMessage() {
		super(new ServerMessageSource());
	}
}
