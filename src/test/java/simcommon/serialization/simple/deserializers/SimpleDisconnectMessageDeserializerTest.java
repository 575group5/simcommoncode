package simcommon.serialization.simple.deserializers;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;

public class SimpleDisconnectMessageDeserializerTest {
	SimpleDisconnectMessageDeserializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleDisconnectMessageDeserializer();
	}
	
	@Test
	public void testReadFromInput(){
		SystemMessage inputMessage = testObj.readFromInput("ClientDis-user|hello");
		assertTrue(inputMessage instanceof ClientDisconnectRequestSystemMessage);
	}
	
	@Test
	public void testCanHandle(){
		assertTrue(testObj.canHandle("ClientDis-user|hello"));
	}

}
