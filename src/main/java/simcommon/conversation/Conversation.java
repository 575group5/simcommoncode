package simcommon.conversation;

import java.util.Collection;

import simcommon.entities.User;
import simcommon.utilities.ParamUtil;

public class Conversation {

	private ConversationId mConversationId;
	private ConversationProperties mConversationProperties;

	public Conversation(ConversationId conversationId, ConversationProperties conversationProperties) {
		ParamUtil.validateParamNotNull("must have ConversationId", conversationId);
		ParamUtil.validateParamNotNull("must have ConversationProperties", conversationProperties);

		mConversationId = conversationId;
		mConversationProperties = conversationProperties;
	}

	public ConversationProperties getProperties() {
		return mConversationProperties;
	}

	public boolean isPublic() {
		return mConversationProperties.isPublic();
	}
	
	public String getName() {
		return mConversationProperties.getName();
	}

	public Collection<User> getParticipants() {
		return mConversationProperties.getParticipants();
	}

	public ConversationId getConversationId() {
		return mConversationId;
	}

	@Override
	public int hashCode() {
		// generated
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mConversationId == null) ? 0 : mConversationId.hashCode());
		result = prime * result + ((mConversationProperties == null) ? 0 : mConversationProperties.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		// generated
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conversation other = (Conversation) obj;
		if (mConversationId == null) {
			if (other.mConversationId != null)
				return false;
		} else if (!mConversationId.equals(other.mConversationId))
			return false;
		if (mConversationProperties == null) {
			if (other.mConversationProperties != null)
				return false;
		} else if (!mConversationProperties.equals(other.mConversationProperties))
			return false;
		return true;
	}
}
