package simcommon.messages.userMessages.origin;

import simcommon.messages.userMessages.IUserMessage;

/**
 * Represents the origin of a {@link IUserMessage} when it comes from the
 * server.
 */
public class ServerOrigin implements IUserMessageOrigin {

}
