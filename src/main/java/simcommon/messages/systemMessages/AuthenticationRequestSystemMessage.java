package simcommon.messages.systemMessages;

import simcommon.authentication.IUserAuthentication;
import simcommon.messages.systemMessages.source.UnknownClientMessageSource;
import simcommon.utilities.ParamUtil;

public class AuthenticationRequestSystemMessage extends SystemMessage {

	private IUserAuthentication mAuthentication;

	public AuthenticationRequestSystemMessage(IUserAuthentication userAuthentication) {
		super(new UnknownClientMessageSource());
		ParamUtil.validateParamNotNull("authentication info cannot be null", userAuthentication);
		mAuthentication = userAuthentication;
	}

	public IUserAuthentication getAuthentication() {
		return mAuthentication;
	}
}
