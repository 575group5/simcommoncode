package simcommon.authentication;

public class UsernameAuthentication implements IUserAuthentication {

	private String mUsername;
	
	public UsernameAuthentication(String username){
		mUsername = username;
	}
	
	public String getUsername(){
		return mUsername;
	}
}