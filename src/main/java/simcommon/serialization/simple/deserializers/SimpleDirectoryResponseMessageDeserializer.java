package simcommon.serialization.simple.deserializers;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationId;
import simcommon.conversation.ConversationProperties;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.DirectoryResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.DeserializationException;
import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.json.ConversationJsonSerializerDeserializer;
import simcommon.serialization.json.JsonDeserializationException;
import simcommon.serialization.simple.serializers.SimpleDirectoryResponseMessageSerializer;

public class SimpleDirectoryResponseMessageDeserializer implements IIndividualMessageDeserializer {

	@Override
	public SystemMessage readFromInput(String input) throws DeserializationException {
		JSONObject jsonObject = new JSONObject(input);

		Collection<Conversation> conversations = new ArrayList<>();
		JSONArray conversationsArray = jsonObject
				.getJSONArray(SimpleDirectoryResponseMessageSerializer.CONVERSATIONS_LIST_KEY);
		for (int i = 0; i < conversationsArray.length(); ++i) {
			JSONObject entry = conversationsArray.getJSONObject(i);
			Conversation conversation;
			try {
				conversation = ConversationJsonSerializerDeserializer.fromJson(entry);
			} catch (JsonDeserializationException e) {
				throw new DeserializationException("couldn't deserialize conversation", e);
			}
			conversations.add(conversation);
		}

		return new DirectoryResponseSystemMessage(conversations);
	}

	@Override
	public boolean canHandle(String input) {
		try {
			JSONObject jsonObject = new JSONObject(input);
			return jsonObject.optBoolean(SimpleDirectoryResponseMessageSerializer.UNIQUE_ID_FIELD, false);
		} catch (JSONException e) {
			return false;
		}
	}

}