package simcommon.messages.systemMessages;

import simcommon.entities.User;
import simcommon.messages.systemMessages.source.UserMessageSource;

public class ClientDisconnectRequestSystemMessage extends SystemMessage {

	public ClientDisconnectRequestSystemMessage(User source) {
		super(new UserMessageSource(source));
	}
}
