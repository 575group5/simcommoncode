package simcommon.entities;

public class StandardServerInfo implements IServerInfo {

	private int mPort;

	public StandardServerInfo(int port) {
		mPort = port;
	}

	public int getPort() {
		return mPort;
	}

}
