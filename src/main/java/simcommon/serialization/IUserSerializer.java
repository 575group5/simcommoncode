package simcommon.serialization;

import java.io.OutputStream;

import simcommon.entities.User;

public interface IUserSerializer {

	public void writeToOutput(User user, OutputStream outputStream);
}
