package simcommon.serialization;

import java.io.OutputStream;

import simcommon.conversation.Conversation;

public interface IConversationSerializer {

	void writeToOutput(Conversation conversation, OutputStream outputStream);
}
