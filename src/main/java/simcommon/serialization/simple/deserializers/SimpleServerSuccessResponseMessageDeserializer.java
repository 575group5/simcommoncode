package simcommon.serialization.simple.deserializers;

import simcommon.messages.systemMessages.GeneralServerSuccessResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IUserDeserializer;

public class SimpleServerSuccessResponseMessageDeserializer implements IIndividualMessageDeserializer {

	protected IUserDeserializer mUserDeserializer;
	
	@Override
	public SystemMessage readFromInput(String input) {
		return new GeneralServerSuccessResponseSystemMessage();
	}

	@Override
	public boolean canHandle(String input) {
		return input.startsWith("MessageSuccess");
	}

}
