package simcommon.serialization.simple.serializers;

import static org.junit.Assert.assertTrue;

import java.io.OutputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import simcommon.authentication.UsernameAuthentication;
import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;

public class SimpleAuthenticateRequestMessageSerializerTest {
	SimpleAuthenticateRequestMessageSerializer testObj;
	
	@Before
	public void setup(){
		testObj = new SimpleAuthenticateRequestMessageSerializer();
	}
	
	@Test
	public void testWriteToOutput(){
		AuthenticationRequestSystemMessage message = new AuthenticationRequestSystemMessage(new UsernameAuthentication("test"));
		OutputStream out = Mockito.mock(OutputStream.class);
		testObj.writeToOutput(message, out);
	}
	
	@Test
	public void testCanHandle(){
		assertTrue(testObj.canHandle(new AuthenticationRequestSystemMessage(new UsernameAuthentication("test"))));
	}

}
