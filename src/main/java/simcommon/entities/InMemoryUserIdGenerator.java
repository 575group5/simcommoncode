package simcommon.entities;

import java.util.UUID;

public class InMemoryUserIdGenerator implements IUserIdGenerator {

	
	public UserId generateUserId(){
		final String id = generateId();
		final UserId userId = new UserId(id);
		
		return userId;
	}
	
	private String generateId(){
		String uniqueID = UUID.randomUUID().toString();
		return uniqueID;
	}
}
