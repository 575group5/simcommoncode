package simcommon.serialization;

import simcommon.messages.systemMessages.SystemMessage;

public interface IIndividualMessageSerializer extends IMessageSerializer {

	boolean canHandle(SystemMessage message);
}
