package simcommon.serialization.simple.serializers;

import java.io.OutputStream;

import simcommon.authentication.UsernameAuthentication;
import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IConversationSerializer;
import simcommon.serialization.IIndividualMessageSerializer;
import simcommon.serialization.IUserMessageSerializer;
import simcommon.serialization.IUserSerializer;

public class SimpleAuthenticateRequestMessageSerializer extends StringMessageSerializer implements IIndividualMessageSerializer {

	protected IUserSerializer mUserSerializer;
	protected IConversationSerializer mConversationSerializer;
	protected IUserMessageSerializer mUserMessageSerializer;

	@Override
	public void writeToOutput(SystemMessage message, OutputStream out) {
		String name = ((UsernameAuthentication) ((AuthenticationRequestSystemMessage) message).getAuthentication())
				.getUsername();
		this.printToStream("AuthReq-name|" + name, out);
	}

	@Override
	public boolean canHandle(SystemMessage message) {
		return message instanceof AuthenticationRequestSystemMessage
				&& ((AuthenticationRequestSystemMessage) message).getAuthentication() instanceof UsernameAuthentication;
	}

}
