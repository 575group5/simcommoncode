package simcommon.serialization.simple.deserializers;

import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.IUserDeserializer;

public class SimpleDirectoryRequestMessageDeserializer implements IIndividualMessageDeserializer {

	protected IUserDeserializer mUserDeserializer;
	
	@Override
	public SystemMessage readFromInput(String input) {
		String userid = input.replaceAll("dirReq-user:", "");
		return new DirectoryRequestSystemMessage(new User(new UserInfo("fixme"), new UserId(userid)));
	}

	@Override
	public boolean canHandle(String input) {

		return input.startsWith("dirReq-user:");
	}

}
