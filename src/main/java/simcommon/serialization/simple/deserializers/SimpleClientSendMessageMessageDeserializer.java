package simcommon.serialization.simple.deserializers;

import org.json.JSONException;
import org.json.JSONObject;

import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.serialization.DeserializationException;
import simcommon.serialization.IIndividualMessageDeserializer;
import simcommon.serialization.json.ConversationJsonSerializerDeserializer;
import simcommon.serialization.json.JsonDeserializationException;
import simcommon.serialization.json.UserJsonSerializerDeserializer;
import simcommon.serialization.json.UserMessageDeserializer;

public class SimpleClientSendMessageMessageDeserializer implements IIndividualMessageDeserializer {

	@Override
	public SystemMessage readFromInput(String input) throws DeserializationException {
		int jsonStart = input.indexOf("|");
		String json = input.substring(jsonStart + 1);
		JSONObject jsonMessage = new JSONObject(json);
		ClientSendMessageRequestSystemMessage message = null;
		try {
			Conversation conversation = ConversationJsonSerializerDeserializer.fromJson(jsonMessage.getJSONObject("mConversation"));
			User user = UserJsonSerializerDeserializer.fromJson(jsonMessage.getJSONObject("mSource").getJSONObject("mUser"));
			IUserMessage userMessage = UserMessageDeserializer.fromJSON(jsonMessage.getJSONObject("mMessage"));
			message = new ClientSendMessageRequestSystemMessage(user, conversation, userMessage);
		} catch (JSONException | JsonDeserializationException e) {
			throw new DeserializationException(e.getMessage());
		}
		return message;
	}

	@Override
	public boolean canHandle(String input) {
		return input.startsWith("ClientSendMessage");
	}

}
