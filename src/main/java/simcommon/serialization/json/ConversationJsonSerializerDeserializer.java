package simcommon.serialization.json;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationId;
import simcommon.conversation.ConversationProperties;
import simcommon.entities.User;

public class ConversationJsonSerializerDeserializer {
	public static JSONObject toJson(Conversation conversation) {
		Gson gson = new GsonBuilder().create();
		return new JSONObject(gson.toJson(conversation));
	}
	public static Conversation fromJson(JSONObject json) throws JsonDeserializationException {
		long id = json.getJSONObject("mConversationId").getLong("mId");
		JSONObject jsonConvProp = json.getJSONObject("mConversationProperties");
		String name = jsonConvProp.optString("mName", null);
		boolean isPublic = jsonConvProp.getBoolean("mIsPublic");
		JSONArray array = jsonConvProp.getJSONArray("mParticipants");
		Collection<User> participants = getParticipants(array);
		return new Conversation(new ConversationId(id), new ConversationProperties(participants, name, isPublic));
	}

	private static Collection<User> getParticipants(JSONArray array) throws JsonDeserializationException {
		Collection<User> particiapnts = new ArrayList<>();
		for (int i = 0; i < array.length(); ++i) {
			JSONObject entry = array.getJSONObject(i);
			particiapnts.add(UserJsonSerializerDeserializer.fromJson(entry));
		}
		return particiapnts;
	}
}
