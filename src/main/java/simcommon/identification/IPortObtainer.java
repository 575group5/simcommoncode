package simcommon.identification;

public interface IPortObtainer {
	
	void obtainPort(IPortObtainedCallback callback);
}
